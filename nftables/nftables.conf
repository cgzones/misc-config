#!/usr/sbin/nft -f

# deletes foreign rules, e.g. for containers
#flush ruleset

# commands
#  - list rules:     nft list ruleset
#  - block ip:       nft add element inet filter blacklist4 { IP }
#                    nft add element inet filter blacklist4 { IP timeout 60s }
#  - list blacklist: nft list set inet filter blacklist4

define interface_inet = eth0
define port_ssh = 12323
#define port_ssh_alias = 5222
define port_quassel = 4242
define port_monit = 2812
define port_rspamdweb = 11334
define local_port_range = 32768 - 60999
define addrv4_vpnnet = 10.1.0.0/16
define addrv6_vpnnet = fd00::/8

destroy table inet myfilter
table inet myfilter {

	set tcp_accepted {
		type inet_service;
		elements = {
			$port_ssh,
			$port_monit,
			$port_quassel,
			smtp, submission, imaps, sieve,
			http, https
		}
	}

	set udp_accepted {
		type inet_service;
		elements = {
			isakmp, ipsec-nat-t
		}
	}

	set blacklist4 { type ipv4_addr; flags timeout; }
	set blacklist6 { type ipv6_addr; flags timeout; }

	chain blacklist_sink {
		counter drop comment "blacklist"
	}

	chain base_checks {
		ip  saddr @blacklist4 counter goto blacklist_sink
		ip6 saddr @blacklist6 counter goto blacklist_sink

		ct state invalid counter drop comment "invalid state"
		ct state { established, related } counter accept comment "established - related"

		iif lo counter accept comment "loopback"
		iif != lo ip  daddr 127.0.0.1/8 counter limit rate 1/second log flags all prefix "nft_lo4 " drop
		iif != lo ip6 daddr ::1/128     counter limit rate 1/second log flags all prefix "nft_lo6 " drop
	}

	chain tcp_checks_sink {
		counter drop comment "tcp flags check sink"
	}

	chain tcp_checks {
		tcp flags & (syn) != (syn) ct state new                            counter goto tcp_checks_sink

		tcp flags & (fin|syn)                 == (fin|syn)                 counter goto tcp_checks_sink
		tcp flags & (syn|rst)                 == (syn|rst)                 counter goto tcp_checks_sink
		tcp flags & (syn|urg)                 == (syn|urg)                 counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == 0                         counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (fin|psh|urg)             counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (fin|syn|rst|psh|ack|urg) counter goto tcp_checks_sink
		tcp flags & (fin|rst)                 == (fin|rst)                 counter goto tcp_checks_sink
		tcp flags & (fin|ack)                 == (fin)                     counter goto tcp_checks_sink
		tcp flags & (ack|psh)                 == (psh)                     counter goto tcp_checks_sink
		tcp flags & (ack|urg)                 == (urg)                     counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (syn|fin|psh|urg)         counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (syn|rst|fin|psh|urg)     counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (fin|syn|rst|ack)         counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (syn|psh)                 counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (syn|psh|ack)             counter goto tcp_checks_sink
		tcp flags & (fin|psh|urg)             == (fin|psh|urg)             counter goto tcp_checks_sink
		tcp flags & (fin|syn|rst|psh|ack|urg) == (fin)                     counter goto tcp_checks_sink
	}

	set tcp_spam4 { type ipv4_addr; flags dynamic; }
	set tcp_spam6 { type ipv6_addr; flags dynamic; }
	set ssh_spam4 { type ipv4_addr; flags dynamic; }
	set ssh_spam6 { type ipv6_addr; flags dynamic; }

	chain tcp_input {
		jump tcp_checks

		#tcp dport { $port_ssh, $port_ssh_alias } add @ssh_spam4 { ip  saddr limit rate over 3/second } log level info flags all prefix "nft_sshspam4 " set add ip  saddr timeout 60s @blacklist4 drop
		#tcp dport { $port_ssh, $port_ssh_alias } add @ssh_spam6 { ip6 saddr limit rate over 3/second } log level info flags all prefix "nft_sshspam6 " set add ip6 saddr timeout 60s @blacklist6 drop
		tcp dport $port_ssh add @ssh_spam4 { ip  saddr limit rate over 3/second } log level info flags all prefix "nft_sshspam4 " set add ip  saddr timeout 180s @blacklist4 drop
		tcp dport $port_ssh add @ssh_spam6 { ip6 saddr limit rate over 3/second } log level info flags all prefix "nft_sshspam6 " set add ip6 saddr timeout 180s @blacklist6 drop

		tcp dport @tcp_accepted tcp sport 1024-65535 accept

		add @tcp_spam4 { ip  saddr limit rate over 100/second } log level info flags all prefix "nft_tcpspam4 " set add ip  saddr timeout 180s @blacklist4 drop
		add @tcp_spam6 { ip6 saddr limit rate over 100/second } log level info flags all prefix "nft_tcpspam6 " set add ip6 saddr timeout 180s @blacklist6 drop

		counter limit rate 50/second reject with icmpx type port-unreachable comment "tcp closed port reject"
		counter limit rate 1/second log flags all prefix "nft_tcpchainend " drop
	}

	set udp_spam4 { type ipv4_addr; flags dynamic; }
	set udp_spam6 { type ipv6_addr; flags dynamic; }

	chain udp_input {
		udp dport @udp_accepted udp sport 1024-65535 accept
		ip  saddr $addrv4_vpnnet udp dport domain udp sport 1024-65535 accept
		ip6 saddr $addrv6_vpnnet udp dport domain udp sport 1024-65535 accept

		add @udp_spam4 { ip  saddr limit rate over 3/second } log level info flags all prefix "nft_udpspam4 " set add ip  saddr timeout 180s @blacklist4 drop
		add @udp_spam6 { ip6 saddr limit rate over 3/second } log level info flags all prefix "nft_udpspam6 " set add ip6 saddr timeout 180s @blacklist6 drop

		counter limit rate 500/second reject with icmpx type port-unreachable comment "udp closed port reject"
		counter limit rate 1/second log flags all prefix "nft_udpchainend " drop
	}

	chain icmpv4_input {
		ip protocol icmp icmp type { echo-request, echo-reply, time-exceeded, parameter-problem, destination-unreachable } counter limit rate 100/second accept comment "icmpv4 accept"

		ip protocol icmp icmp type { timestamp-request } counter drop

		counter limit rate 1/second log flags all prefix "nft_icmp4chainend " drop
	}

	chain icmpv6_input {
		ip6 nexthdr icmpv6 icmpv6 type { echo-request, echo-reply, time-exceeded, parameter-problem, destination-unreachable, packet-too-big, nd-router-solicit, mld-listener-query } counter limit rate 100/second accept comment "icmpv6 accept"
		ip6 nexthdr icmpv6 icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit, nd-router-advert } ip6 hoplimit 1 limit rate 100/second accept
		ip6 nexthdr icmpv6 icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit, nd-router-advert } ip6 hoplimit 255 limit rate 100/second accept

		counter limit rate 1/second log flags all prefix "nft_icmpv6chainend " drop
	}

	chain output {
		type filter hook output priority 0; policy accept;
	}

	chain forward {
		type filter hook forward priority 0; policy drop;

		counter ip  saddr $addrv4_vpnnet oif $interface_inet accept comment "forward ipv4 vpn to net"
		counter ip  daddr $addrv4_vpnnet iif $interface_inet accept comment "forward ipv4 net to vpn"
		counter ip6 saddr $addrv6_vpnnet oif $interface_inet accept comment "forward ipv6 vpn to net"
		counter ip6 daddr $addrv6_vpnnet iif $interface_inet accept comment "forward ipv6 net to vpn"

		ip6 nexthdr icmpv6 icmpv6 type { echo-request } counter limit rate 100/second accept comment "forward icmpv6 accept"

		counter limit rate 1/second log flags all prefix "nft_fwdchainend " reject
	}

	chain input {
		type filter hook input priority 0; policy drop;

		counter comment "input"

		jump base_checks

		ip protocol vmap { tcp : goto tcp_input, udp : goto udp_input, icmp   : goto icmpv4_input }
		ip6 nexthdr vmap { tcp : goto tcp_input, udp : goto udp_input, icmpv6 : goto icmpv6_input }

		ip  protocol sctp drop comment "SCTP"
		ip6 nexthdr  sctp drop comment "SCTP"

		#ip protocol  4 counter drop comment "drop IP in IP (encapsulation)"
		#ip protocol 41 counter drop comment "drop IPv6 Encapsulation"
		ip protocol 47 counter drop comment "drop Generic Routing Encapsulation"

		counter limit rate 1/second log flags all prefix "nft_inputendchain " drop
	}
}

destroy table ip mynat
table ip mynat {
	#chain prerouting {
	#	type nat hook prerouting priority -100; policy accept;
	#
	#	tcp dport $port_ssh_alias redirect to $port_ssh comment "ipv4 ssh port redirection for public networks"
	#}

	chain postrouting {
		type nat hook postrouting priority 100; policy accept;

		ip saddr $addrv4_vpnnet oif $interface_inet counter masquerade fully-random comment "masquerade ipv4"
	}
}

destroy table ip6 mynat6
table ip6 mynat6 {
	#chain prerouting {
	#	type nat hook prerouting priority -100; policy accept;
	#
	#	tcp dport $port_ssh_alias redirect to $port_ssh comment "ipv6 ssh port redirection for public networks"
	#}

	chain postrouting {
		type nat hook postrouting priority 100; policy accept;

		ip6 saddr $addrv6_vpnnet oif $interface_inet counter masquerade fully-random comment "masquerade ipv6"
	}
}

destroy table inet mysecmark
table inet mysecmark {

	secmark dhcpc_client		{ "system_u:object_r:dhcpc_client_packet_t:s0" }
	secmark dhcpc_server		{ "system_u:object_r:dhcpc_server_packet_t:s0" }
	secmark dhcpd_client		{ "system_u:object_r:dhcpd_client_packet_t:s0" }
	secmark dhcpd_server		{ "system_u:object_r:dhcpd_server_packet_t:s0" }
	secmark dns_client		{ "system_u:object_r:dns_client_packet_t:s0" }
	secmark dns_lo_client		{ "system_u:object_r:dns_lo_client_packet_t:s0" }
	secmark dns_lo_server		{ "system_u:object_r:dns_lo_server_packet_t:s0" }
	secmark dns_server		{ "system_u:object_r:dns_server_packet_t:s0" }
	secmark git_client		{ "system_u:object_r:git_client_packet_t:s0" }
	secmark http_client		{ "system_u:object_r:http_client_packet_t:s0" }
	secmark http_server		{ "system_u:object_r:http_server_packet_t:s0" }
	secmark icmp_packet		{ "system_u:object_r:icmp_packet_t:s0" }
	secmark icmp_lo_packet		{ "system_u:object_r:icmp_lo_packet_t:s0" }
	secmark ipsecnat_client		{ "system_u:object_r:ipsecnat_client_packet_t:s0" }
	secmark ipsecnat_server		{ "system_u:object_r:ipsecnat_server_packet_t:s0" }
	secmark ircd_client		{ "system_u:object_r:ircd_client_packet_t:s0" }
	secmark isakmp_client		{ "system_u:object_r:isakmp_client_packet_t:s0" }
	secmark isakmp_server		{ "system_u:object_r:isakmp_server_packet_t:s0" }
	secmark monit_client		{ "system_u:object_r:monit_client_packet_t:s0" }
	secmark monit_server		{ "system_u:object_r:monit_server_packet_t:s0" }
	secmark munin_httpd_lo_client	{ "system_u:object_r:munin_httpd_lo_client_packet_t:s0" }
	secmark munin_httpd_lo_server	{ "system_u:object_r:munin_httpd_lo_server_packet_t:s0" }
	secmark munin_lo_client		{ "system_u:object_r:munin_lo_client_packet_t:s0" }
	secmark munin_lo_server		{ "system_u:object_r:munin_lo_server_packet_t:s0" }
	secmark mysql_lo_client		{ "system_u:object_r:mysqld_lo_client_packet_t:s0" }
	secmark mysql_lo_server		{ "system_u:object_r:mysqld_lo_server_packet_t:s0" }
	secmark ntp_client		{ "system_u:object_r:ntp_client_packet_t:s0" }
	secmark pgpkeyserver_client	{ "system_u:object_r:pgpkeyserver_client_packet_t:s0" }
	secmark pihole_lo_client	{ "system_u:object_r:pihole_lo_client_packet_t:s0" }
	secmark pihole_lo_server	{ "system_u:object_r:pihole_lo_server_packet_t:s0" }
	secmark pop_client		{ "system_u:object_r:pop_client_packet_t:s0" }
	secmark pop_lo_client		{ "system_u:object_r:pop_lo_client_packet_t:s0" }
	secmark pop_lo_server		{ "system_u:object_r:pop_lo_server_packet_t:s0" }
	secmark pop_server		{ "system_u:object_r:pop_server_packet_t:s0" }
	secmark quassel_server		{ "system_u:object_r:quassel_server_packet_t:s0" }
	secmark redis_lo_client		{ "system_u:object_r:redis_lo_client_packet_t:s0" }
	secmark redis_lo_server		{ "system_u:object_r:redis_lo_server_packet_t:s0" }
	secmark rndc_lo_client		{ "system_u:object_r:rndc_lo_client_packet_t:s0" }
	secmark rndc_lo_server		{ "system_u:object_r:rndc_lo_server_packet_t:s0" }
	secmark rspamd_client		{ "system_u:object_r:rspamd_client_packet_t:s0" }
	secmark rspamd_lo_client	{ "system_u:object_r:rspamd_lo_client_packet_t:s0" }
	secmark rspamd_lo_server	{ "system_u:object_r:rspamd_lo_server_packet_t:s0" }
	secmark rspamdweb_client	{ "system_u:object_r:rspamdweb_client_packet_t:s0" }
	secmark rspamdweb_server	{ "system_u:object_r:rspamdweb_server_packet_t:s0" }
	secmark sieve_client		{ "system_u:object_r:sieve_client_packet_t:s0" }
	secmark sieve_server		{ "system_u:object_r:sieve_server_packet_t:s0" }
	secmark smtp_client		{ "system_u:object_r:smtp_client_packet_t:s0" }
	secmark smtp_server		{ "system_u:object_r:smtp_server_packet_t:s0" }
	secmark ssh_client		{ "system_u:object_r:ssh_client_packet_t:s0" }
	secmark ssh_server		{ "system_u:object_r:ssh_server_packet_t:s0" }
	secmark ssh_tarpit_server	{ "system_u:object_r:ssh_tarpit_server_packet_t:s0" }
	secmark tor_client		{ "system_u:object_r:tor_client_packet_t:s0" }

	map secmapping_in {
		type inet_service : secmark
		elements = {
			bootpc		: "dhcpc_server",
			bootps		: "dhcpd_server",
			domain		: "dns_server",
			http		: "http_server",
			https		: "http_server",
			ipsec-nat-t	: "ipsecnat_server",
			isakmp		: "isakmp_server",
			$port_monit	: "monit_server",
			imaps		: "pop_server",
			$port_quassel	: "quassel_server",
			$port_rspamdweb	: "rspamdweb_server",
			sieve		: "sieve_server",
			smtp		: "smtp_server",
			submission	: "smtp_server",
			ssh		: "ssh_tarpit_server",
			$port_ssh	: "ssh_server",
		}
	}

	map secmapping_lo_in {
		type inet_service : secmark
		elements = {
			domain		: "dns_lo_server",
			4948		: "munin_httpd_lo_server",
			munin		: "munin_lo_server",
			533		: "dns_lo_server",
			mysql		: "mysql_lo_server",
			4711		: "pihole_lo_server",
			6379		: "redis_lo_server",
			8953		: "rndc_lo_server",
			11332		: "rspamd_lo_server",
			11333		: "rspamd_lo_server",
			11335		: "rspamd_lo_server",
		}
	}

	map secmapping_out {
		type inet_service : secmark
		elements = {
			bootpc		: "dhcpc_client",
			bootps		: "dhcpd_client",
			domain		: "dns_client",
			git		: "git_client",
			http		: "http_client",
			https		: "http_client",
			ipsec-nat-t	: "ipsecnat_client",
			6661		: "ircd_client",
			6665		: "ircd_client",
			6697		: "ircd_client",
			ircd		: "ircd_client",
			7000		: "ircd_client",
			isakmp		: "isakmp_client",
			$port_monit	: "monit_client",
			ntp		: "ntp_client",
			11371		: "pgpkeyserver_client",
			imaps		: "pop_client",
			11335		: "rspamd_client",
			$port_rspamdweb	: "rspamdweb_client",
			sieve		: "sieve_client",
			submission	: "smtp_client",
			smtp		: "smtp_client",
			ssh		: "ssh_client",
			9050		: "tor_client",
			9150		: "tor_client",
		}
	}

	map secmapping_lo_out {
		type inet_service : secmark
		elements = {
			domain		: "dns_lo_client",
			4948		: "munin_httpd_lo_client",
			munin		: "munin_lo_client",
			533		: "dns_lo_client",
			mysql		: "mysql_lo_client",
			4711		: "pihole_lo_client",
			imaps		: "pop_lo_client",
			6379		: "redis_lo_client",
			8953		: "rndc_lo_client",
			11332		: "rspamd_lo_client",
			11333		: "rspamd_lo_client",
			11335		: "rspamd_lo_client",
		}
	}

	chain input {
		type filter hook input priority -225;

		# get label for est/rel packets from connection
		ct state established,related meta secmark set ct secmark

		# label new incoming packets
		ct state new meta secmark set tcp dport map @secmapping_in
		ct state new meta secmark set udp dport map @secmapping_in
		ip protocol icmp meta secmark set "icmp_packet"
		ip6 nexthdr icmpv6 meta secmark set "icmp_packet"

		# fix loopback labels
		iif lo meta secmark set tcp dport map @secmapping_in
		iif lo meta secmark set tcp dport map @secmapping_lo_in
		iif lo meta secmark set udp dport map @secmapping_in
		iif lo meta secmark set udp dport map @secmapping_lo_in
		iif lo meta secmark set tcp sport map @secmapping_out
		iif lo meta secmark set tcp sport map @secmapping_lo_out
		iif lo meta secmark set udp sport map @secmapping_out
		iif lo meta secmark set udp sport map @secmapping_lo_out
		iif lo ip protocol icmp meta secmark set "icmp_lo_packet"
		iif lo ip6 nexthdr icmpv6 meta secmark set "icmp_lo_packet"

		# save label onto connection
		ct state new ct secmark set meta secmark
		#iif lo       ct secmark set meta secmark
	}

	chain output {
		type filter hook output priority 225;

		# get label for est/rel packets from connection
		ct state established,related meta secmark set ct secmark

		# label new outgoing packets
		ct state new meta secmark set tcp dport map @secmapping_out
		ct state new meta secmark set udp dport map @secmapping_out
		ip protocol icmp meta secmark set "icmp_packet"
		ip6 nexthdr icmpv6 meta secmark set "icmp_packet"

		# fix loopback labels
		oif lo meta secmark set tcp dport map @secmapping_out
		oif lo meta secmark set tcp dport map @secmapping_lo_out
		oif lo meta secmark set udp dport map @secmapping_out
		oif lo meta secmark set udp dport map @secmapping_lo_out
		oif lo meta secmark set tcp sport map @secmapping_in
		oif lo meta secmark set tcp sport map @secmapping_lo_in
		oif lo meta secmark set udp sport map @secmapping_in
		oif lo meta secmark set udp sport map @secmapping_lo_in
		oif lo ip protocol icmp meta secmark set "icmp_lo_packet"
		oif lo ip6 nexthdr icmpv6 meta secmark set "icmp_lo_packet"

		# save label onto connection
		ct state new ct secmark set meta secmark
		#oif lo       ct secmark set meta secmark
	}

}
